//
//  NotificationView.swift
//  Beats WatchKit Extension
//
//  Created by Samuel Putra on 19/08/21.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
