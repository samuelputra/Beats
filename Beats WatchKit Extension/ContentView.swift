//
//  ContentView.swift
//  Beats WatchKit Extension
//
//  Created by Samuel Putra on 19/08/21.
//

import SwiftUI

struct ContentView: View {
    
    @State var bpm = 40.0
    @State var playMetronome = false
    
    var body: some View {
        
//MARK: VStack
        VStack{
            // Title
//            Text("Beats")
//                .font(.title2)
//                .fontWeight(.bold)
//                .multilineTextAlignment(.leading)
//
//                .offset(x: -45, y: 20)
//                .padding()
            
        // MARK: HStack ("BPM, BPMLabel, Button")
            HStack{
                // MARK: Button (-)
                Button(action: {
                        bpm = bpm - 1
                }){
                    Image(systemName: "minus")
                        .font(.title3)
                        .foregroundColor(Color.blue)
                }
                
                    //Frame Button "-"
                    .frame(width: 45, height: 45)
                
                
                // MARK: BPM Label
                VStack{
                    
                    // Text BPM
                    Text("\(Int(bpm))")
                        .font(.system(size: 32))
                        .fontWeight(.semibold)
                        .multilineTextAlignment(.center)
                    
                        // MARK: Digital Crown Rotation
                            .focusable(true)
                            .digitalCrownRotation($bpm)
                            .digitalCrownRotation($bpm, from: 40.0, through: 218.0, by: 1, sensitivity: .medium, isContinuous: true, isHapticFeedbackEnabled: false)
                                //"Kalau tidak mau Haptic, bisa dihapus atau "FALSE"
                    
                    // Text BPMLabel
                    Text("bpm")
                        .font(.subheadline)
                        .fontWeight(.regular)
                        .multilineTextAlignment(.center)
                
                }//~BPM Detail~
                
                // Frame Detail BPM
                .frame(width: 60.0, height: 60.0)
                .padding(.horizontal, 4.0)
    
                
                // MARK: Button (+)
                Button(action: {
                        bpm = bpm + 1
                }){
                    Image(systemName: "plus")
                        .font(.title3)
                        .foregroundColor(Color.blue)
                }
                
                    //Frame Button "+"
                    .frame(width: 45, height: 45)
        
                
            }//~HStack~
            
            .offset(y: 30)
            .padding()
            
        // MARK: Garis Pemisah
            Divider()
            .offset(y: 35)
            .padding()
            
        // MARK: Navigation Link
            NavigationLink(
                destination: PlayView(playMetronome: playMetronome, playviewbpm: $bpm),
                isActive: $playMetronome,
                label: {
                    Image(systemName: "play.fill")
                        .font(.body)
                        .foregroundColor(Color.blue)
                    Text("Play")
                        .fontWeight(.medium)
                })
                
                .offset(y: 20)
                .padding()
            
        }//~VStack~

// MARK: Navigation Title
        .navigationTitle("Beats")
        
    }//~Struct:View~
    
    struct ContentView_Previews: PreviewProvider {
        static var previews: some View {
            ContentView()
        }
    }
}
