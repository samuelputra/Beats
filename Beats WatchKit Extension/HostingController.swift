//
//  HostingController.swift
//  Beats WatchKit Extension
//
//  Created by Samuel Putra on 19/08/21.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<ContentView> {
    override var body: ContentView {
        return ContentView()
    }
}
