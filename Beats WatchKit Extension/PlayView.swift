//
//  PlayView.swift
//  Beats WatchKit Extension
//
//  Created by Samuel Putra on 20/08/21.
//

import AVFoundation
import SwiftUI

struct PlayView: View {
    
    @State var playMetronome:Bool
    @Binding var playviewbpm:Double
    @Environment(\.presentationMode) var stopMetronome
    
    @State var circle1 = Color.white
    @State var circle2 = Color.white
    @State var circle3 = Color.white
    @State var circle4 = Color.white
    
    @State var beatCounter = 0
    @State var beatAudio: AVAudioPlayer!
    
    // Timer for Feedback and Sound
    @State var timer = Timer.publish(every: 1.500, on: .main, in: .common).autoconnect()
    
    var body: some View {

// MARK: VStack
        VStack{
            
            //BPM View
            Text("\(Int(playviewbpm))")
                .font(.largeTitle)
                .fontWeight(.bold)
                .multilineTextAlignment(.center)
            
            //BPM Label
            Text("bpm")
                .font(.title3)
                .fontWeight(.regular)
                .multilineTextAlignment(.center)
            
            //Circle
            HStack{
                Circle()
                    .fill(circle1)
                    .frame(width: 15, height: 15, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                
                Circle()
                    .fill(circle2)
                    .frame(width: 15, height: 15, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                
                Circle()
                    .fill(circle3)
                    .frame(width: 15, height: 15, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                
                Circle()
                    .fill(circle4)
                    .frame(width: 15, height: 15, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            }
            
            .offset(y: 10)
        
            //MARK: Button Stop
            Button(action: {
                stopMetronome.wrappedValue.dismiss()
            }){
                Image(systemName:"stop.fill")
                    .foregroundColor(.red)
                Text("Stop")
                    .fontWeight(.medium)
                    .foregroundColor(.white)
            }
           
            .offset(y:31)
            .padding()
            
            //MARK: Nav. Bar
                // Hide Nav Bar
                .navigationBarHidden(true)
            
        }
        
        //MARK: Timer Receive
        .onReceive(timer, perform: { _ in
            circle1 = Color.white
            circle2 = Color.white
            circle3 = Color.white
            circle4 = Color.white
            beatCounter = beatCounter + 1
            if beatCounter == 1 || beatCounter == 5 {
                WKInterfaceDevice.current().play(.start)
                circle1 = Color.red
                beatCounter = 1
            }
            else if beatCounter == 2 {
                playbpm("Start.mp3")
                circle2 = Color.red
            }
            else if beatCounter == 3 {
                playbpm("Start.mp3")
                circle3 = Color.red
            }
            else if beatCounter == 4 {
                playbpm("Start.mp3")
                circle4 = Color.red
            }
            
        })
        .onAppear(perform: {
            timer = Timer.publish(every: 60/playviewbpm, on: .main, in: .common).autoconnect()
        })
        }
    func playbpm(_ soundFileName : String) {
        guard let soundURL = Bundle.main.url(forResource:"Start", withExtension:"mp3") else {
                fatalError("Unable to find \(soundFileName) in bundle")
            }
            do {
                beatAudio = try AVAudioPlayer(contentsOf: soundURL)
            } catch {
                print(error.localizedDescription)
            }
            beatAudio.play()
        }//~Func. Play BPM~
}//~PlayView~
